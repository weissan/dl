#!/bin/bash
set -xeu
date
[ -f ~/.last_cb_id ] && last_cb_id=`cat ~/.last_cb_id` || last_cb_id=0
new_cb=$(curl -s "https://www.jisilu.cn/data/calendar/get_calendar_data/?qtype=CNV&start=`date +%s`" | jq -c "[.[] | select ((.title | contains(\"申购日\")) and (.id|sub(\"^CNV\";\"\")|tonumber > $last_cb_id))]")
if [ "$new_cb" != "[]" ]
then
  curl -w "\n" 'https://api.telegram.org/bot959949551:AAFc0FNzEaVow1JnIYyP3KSKD4g2HTp5AJI/sendMessage' -X POST -d "chat_id=-1001307438877&text=`echo -n $new_cb | jq -c '.[]'`"
  new_cb_id=$(echo -n $new_cb | jq -c '[.[] | .id|sub("^CNV";"")|tonumber] | max')
  echo -n $new_cb_id > ~/.last_cb_id;
fi;
