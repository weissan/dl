#!/bin/sh
set -o errexit
set -o nounset
set -x

#
rm -rf $HOME/ffmpeg_build $HOME/bin/{ffmpeg,ffprobe,ffserver,lame,x264,x265}

#Get the Dependencies
yum install -y yum-utils
yum-config-manager --add-repo http://www.nasm.us/nasm.repo
yum install -y autoconf automake bzip2 cmake freetype-devel gcc gcc-c++ git libtool make mercurial nasm pkgconfig zlib-devel

#Get source
if [ ! -d "$HOME/ffmpeg_sources" ] ; then
    git clone https://github.com/FFmpeg/FFmpeg.git $HOME/ffmpeg_sources
fi

#yasm
cd $HOME/ffmpeg_sources
wget -nc http://www.tortall.net/projects/yasm/releases/yasm-1.3.0.tar.gz
tar xzvf yasm-1.3.0.tar.gz
cd yasm-1.3.0
./configure --prefix="$HOME/ffmpeg_build" --bindir="$HOME/bin"
make
make install

#nasm
cd /etc/yum.repos.d/
wget -nc http://nasm.us/nasm.repo
yum install -y nasm

#libx264 
cd $HOME/ffmpeg_sources
if [ ! -d "./x264" ] ; then
    git clone --depth 1 http://git.videolan.org/git/x264
fi
cd x264
PKG_CONFIG_PATH="$HOME/ffmpeg_build/lib/pkgconfig" ./configure --prefix="$HOME/ffmpeg_build" --bindir="$HOME/bin" --enable-static
make
make install

#libx265
cd $HOME/ffmpeg_sources
if [ ! -d "./x265" ] ; then
    hg clone https://bitbucket.org/multicoreware/x265
fi
cd $HOME/ffmpeg_sources/x265/build/linux
cmake -G "Unix Makefiles" -DCMAKE_INSTALL_PREFIX="$HOME/ffmpeg_build" -DENABLE_SHARED:bool=off ../../source
make
make install

#libfdk_aac 
cd $HOME/ffmpeg_sources
if [ ! -d "./fdk-aac" ] ; then
    git clone --depth 1 https://github.com/mstorsjo/fdk-aac
fi
cd fdk-aac
autoreconf -fiv
./configure --prefix="$HOME/ffmpeg_build" --disable-shared
make
make install

#libmp3lame
cd $HOME/ffmpeg_sources
wget -nc http://downloads.sourceforge.net/project/lame/lame/3.99/lame-3.99.5.tar.gz
tar xzvf lame-3.99.5.tar.gz
cd lame-3.99.5
./configure --prefix="$HOME/ffmpeg_build" --bindir="$HOME/bin" --disable-shared --enable-nasm
make
make install

#libopus 
cd $HOME/ffmpeg_sources
wget -nc https://archive.mozilla.org/pub/opus/opus-1.1.5.tar.gz
tar xzvf opus-1.1.5.tar.gz
cd opus-1.1.5
./configure --prefix="$HOME/ffmpeg_build" --disable-shared
make
make install

#libogg 
cd $HOME/ffmpeg_sources
wget -nc http://downloads.xiph.org/releases/ogg/libogg-1.3.2.tar.gz
tar xzvf libogg-1.3.2.tar.gz
cd libogg-1.3.2
./configure --prefix="$HOME/ffmpeg_build" --disable-shared
make
make install

#libvorbis
cd $HOME/ffmpeg_sources
wget -nc http://downloads.xiph.org/releases/vorbis/libvorbis-1.3.4.tar.gz
tar xzvf libvorbis-1.3.4.tar.gz
cd libvorbis-1.3.4
./configure --prefix="$HOME/ffmpeg_build" --with-ogg="$HOME/ffmpeg_build" --disable-shared
make
make install

#libvpx
cd $HOME/ffmpeg_sources
if [ ! -d "./libvpx" ] ; then
    git clone --depth 1 https://chromium.googlesource.com/webm/libvpx.git
fi
cd libvpx
./configure --prefix="$HOME/ffmpeg_build" --disable-examples --disable-unit-tests --enable-vp9-highbitdepth --as=yasm
PATH="$HOME/bin:$PATH" make
make install

#FFmpeg
cd $HOME/ffmpeg_sources
wget -nc http://ffmpeg.org/releases/ffmpeg-snapshot.tar.bz2
tar xjvf ffmpeg-snapshot.tar.bz2
cd ffmpeg
PKG_CONFIG_PATH="$HOME/ffmpeg_build/lib/pkgconfig" ./configure --prefix="$HOME/ffmpeg_build" \
  --extra-cflags="-I$HOME/ffmpeg_build/include" --extra-ldflags="-L$HOME/ffmpeg_build/lib -ldl" \
  --bindir="$HOME/bin" --pkg-config-flags="--static" \
  --enable-gpl \
  --enable-libfdk_aac \
  --enable-libfreetype \
  --enable-libmp3lame \
  --enable-libopus \
  --enable-libvorbis \
  --enable-libvpx \
  --enable-libx264 \
  --enable-libx265 \
  --enable-nonfree
make
make install
hash -r
