#!/bin/bash
set -o errexit
set -o nounset
set -x

NODE="controller,compute"
RABBIT_PASS=123456
ADMIN_PASS=123456
ROOT_DBPASS=123456
KEYSTONE_DBPASS=123456
MYUSER_PASS=123456
GLANCE_DBPASS=123456
GLANCE_PASS=123456
NOVA_DBPASS=123456
NOVA_PASS=123456
PLACEMENT_PASS=123456
NEUTRON_PASS=123456
METADATA_SECRET=123456

#prerequisites
[[ `uname -r` = *el7* ]] && { echo "Start to install openstack"; } || { echo "Please run in Centos 7";exit; }
systemctl stop firewalld.service
systemctl disable firewalld.service
firewall-cmd --state
sed -i "/^SELINUX=/c SELINUX=disabled" /etc/selinux/config
sed -i "/^SELINUXTYPE=/c SELINUXTYPE=disabled/" /etc/selinux/config
grep --color=auto "^SELINUX" /etc/selinux/config
setenforce 0

#1 Environment
#1.1 Host networking
PROVIDER_INTERFACE=enp4s0
PROVIDER_IP=192.168.1.103
MANAGEMENT_INTERFACE=n2n0
MANAGEMENT_IP=10.0.0.2
hostnamectl set-hostname `awk -F, "{print $1}" <<< $NODE`
#cp hosts to /etc/hosts

#1.2 Network Time Protocol (NTP)
yum install -y chrony
if grep -q controller <<<$NODE; then
else
  sed -i "/^server/d" tmp.txt
  sed -i "2aserver controller iburst" test.txt
fi
systemctl enable chronyd.service
systemctl restart chronyd.service

#1.3 OpenStack packages
yum install -y centos-release-openstack-stein
#yum upgrade
yum install python-openstackclient
yum install openstack-selinux

#1.4 SQL database
if grep -q controller <<<$NODE; then
yum install -y mariadb mariadb-server python2-PyMySQL
mkdir -p /etc/my.cnf.d
cat <<EOF > /etc/my.cnf.d/openstack.cnf 
[mysqld]
bind-address = 0.0.0.0

default-storage-engine = innodb
innodb_file_per_table = on
max_connections = 4096
collation-server = utf8_general_ci
character-set-server = utf8
EOF
systemctl enable mariadb.service
systemctl restart mariadb.service

mysqladmin -u root password $ROOT_DBPASS || echo -n
mysql -u root -p$ROOT_DBPASS <<EOF
grant all privileges on *.* to "root"@"%" identified by "$ROOT_DBPASS";
grant all privileges on *.* to "root"@"localhost" identified by "$ROOT_DBPASS";
EOF
fi

#1.5 Message queue
if grep -q controller <<<$NODE; then
yum install -y rabbitmq-server
systemctl enable rabbitmq-server.service
systemctl restart rabbitmq-server.service
rabbitmqctl add_user openstack $RABBIT_PASS
rabbitmqctl set_permissions openstack ".*" ".*" ".*"
fi

#1.6 Memcached
if grep -q controller <<<$NODE; then
yum install -y memcached python-memcached
sed -i "/^OPTIONS=/c OPTIONS="-l 0.0.0.0" /etc/sysconfig/memcached
systemctl enable memcached.service
systemctl restart memcached.service
fi

#2 Install OpenStack services
#2.1 Identity service
if grep -q controller <<<$NODE; then
mysql -u root -p$ROOT_DBPASS <<EOF
CREATE DATABASE IF NOT EXISTS `keystone`;
grant all privileges on keystone.* to "keystone"@%identified by "$KEYSTONE_DBPASS";  
grant all privileges on keystone.* to "keystone"@"localhost" identified by "$KEYSTONE_DBPASS";  
EOF
yum install openstack-keystone httpd mod_wsgi
sed -i "/^connection ?=/c connection = mysql+pymysql:\/\/keystone:$KEYSTONE_DBPASS@controller\/keystone" /etc/keystone/keystone.conf
sed -i "/^provider ?=/c provider = fernet" /etc/keystone/keystone.conf
su -s /bin/sh -c "keystone-manage db_sync" keystone
keystone-manage fernet_setup --keystone-user keystone --keystone-group keystone
keystone-manage credential_setup --keystone-user keystone --keystone-group keystone
keystone-manage bootstrap --bootstrap-password $ADMIN_PASS \
  --bootstrap-admin-url http://controller:5000/v3/ \
  --bootstrap-internal-url http://controller:5000/v3/ \
  --bootstrap-public-url http://controller:5000/v3/ \
  --bootstrap-region-id RegionOne
sed -i "/^ServerName/c ServerName controller" /etc/httpd/conf/httpd.conf
ln -s /usr/share/keystone/wsgi-keystone.conf /etc/httpd/conf.d/
systemctl enable httpd.service
systemctl restart httpd.service
source admin-openrc
openstack domain create --description "An Example Domain" example
openstack project create --domain default --description "Service Project" service
openstack project create --domain default --description "Demo Project" myproject
openstack user create --domain default --password $MYUSER_PASS myuser
openstack role create myrole
openstack role add --project myproject --user myuser myrole
unset OS_AUTH_URL OS_PASSWORD
openstack --os-auth-url http://controller:5000/v3 \
  --os-project-domain-name Default --os-user-domain-name Default \
  --os-project-name admin --os-username admin token issue
openstack --os-auth-url http://controller:5000/v3 \
  --os-project-domain-name Default --os-user-domain-name Default \
  --os-project-name myproject --os-username myuser token issue
fi

#2.2 Image service
if grep -q controller <<<$NODE; then
mysql -u root -p$ROOT_DBPASS <<EOF
CREATE DATABASE IF NOT EXISTS `glance`;
grant all privileges on glance.* to "glance"@%identified by "$GLANCE_DBPASS";  
grant all privileges on glance.* to "glance"@"localhost" identified by "$GLANCE_DBPASS";  
EOF
source admin-openrc
#Create the glance user:
openstack user create --domain default --password $GLANCE_PASS glance
#Add the admin role to the glance user and service project:
openstack role add --project service --user glance admin
#Create the glance service entity:
openstack service create --name glance \
  --description "OpenStack Image" image
#Create the Image service API endpoints:
openstack endpoint create --region RegionOne \
  image public http://controller:9292
yum install -y openstack-glance
sed -i "/^connection ?=/c connection = mysql+pymysql:\/\/glance:$GLANCE_DBPASS@controller\/glance" /etc/glance/glance-api.conf
STARTING="^\[keystone_authtoken\]"
APPEND="
www_authenticate_uri  = http://controller:5000
auth_url = http://controller:5000
memcached_servers = controller:11211
auth_type = password
project_domain_name = Default
user_domain_name = Default
project_name = service
username = glance
password = GLANCE_PASS
"
sed -i "/$STARTING/,/^#/{/^[^#]/{//!d}}" /etc/glance/glance-api.conf
echo -n $APPEND | sed -i "/$STARTING/r /dev/stdin" /etc/glance/glance-api.conf
STARTING="^\[paste_deploy\]"
APPEND="
flavor = keystone
"
sed -i "/$STARTING/,/^#/{/^[^#]/{//!d}}" /etc/glance/glance-api.conf
echo -n $APPEND | sed -i "/$STARTING/r /dev/stdin" /etc/glance/glance-api.conf
STARTING="^\[glance_store\]"
APPEND="
stores = file,http
default_store = file
filesystem_store_datadir = /var/lib/glance/images/
"
sed -i "/$STARTING/,/^#/{/^[^#]/{//!d}}" /etc/glance/glance-api.conf
echo -n $APPEND | sed -i "/$STARTING/r /dev/stdin" /etc/glance/glance-api.conf
#Populate the Image service database:
su -s /bin/sh -c "glance-manage db_sync" glance
systemctl enable openstack-glance-api.service
systemctl start openstack-glance-api.service

wget -nc http://download.cirros-cloud.net/0.4.0/cirros-0.4.0-x86_64-disk.img
openstack image create "cirros" \
  --file cirros-0.4.0-x86_64-disk.img \
  --disk-format qcow2 --container-format bare \
  --public
openstack image list
fi

#2.3 Compute service
if grep -q controller <<<$NODE; then
mysql -u root -p$ROOT_DBPASS <<EOF
CREATE DATABASE IF NOT EXISTS `nova_api`;
CREATE DATABASE IF NOT EXISTS `nova`;
CREATE DATABASE IF NOT EXISTS `nova_cell0`;
grant all privileges on nova.* to "nova"@%identified by "$NOVA_DBPASS";  
grant all privileges on nova.* to "nova"@"localhost" identified by "$NOVA_DBPASS";  
grant all privileges on nova_api.* to "nova"@%identified by "$NOVA_DBPASS";  
grant all privileges on nova_api.* to "nova"@"localhost" identified by "$NOVA_DBPASS";  
grant all privileges on nova_cell0.* to "nova"@%identified by "$NOVA_DBPASS";  
grant all privileges on nova_cell0.* to "nova"@"localhost" identified by "$NOVA_DBPASS";  
EOF
source admin-openrc
openstack user create --domain default --password $NOVA_PASS nova
openstack role add --project service --user nova admin
openstack service create --name nova --description "OpenStack Compute" compute
openstack endpoint create --region RegionOne compute internal http://controller:8774/v2.1
openstack endpoint create --region RegionOne compute admin http://controller:8774/v2.1
yum install openstack-nova-api openstack-nova-conductor openstack-nova-novncproxy openstack-nova-scheduler
else
yum install -y openstack-nova-compute
fi
STARTING="^\[DEFAULT\]"
APPEND="
enabled_apis = osapi_compute,metadata
transport_url = rabbit://openstack:$RABBIT_PASS@controller
my_ip = $MANAGEMENT_IP
use_neutron = true
firewall_driver = nova.virt.firewall.NoopFirewallDriver
"
sed -i "/$STARTING/,/^#/{/^[^#]/{//!d}}" /etc/nova/nova.conf
echo -n $APPEND | sed -i "/$STARTING/r /dev/stdin" /etc/nova/nova.conf

if grep -q controller <<<$NODE; then
STARTING="^\[api_database\]"
APPEND="
connection = mysql+pymysql://nova:$NOVA_DBPASS@controller/nova_api
"
sed -i "/$STARTING/,/^#/{/^[^#]/{//!d}}" /etc/nova/nova.conf
echo -n $APPEND | sed -i "/$STARTING/r /dev/stdin" /etc/nova/nova.conf
STARTING="^\[database\]"
APPEND="
connection = mysql+pymysql://nova:$NOVA_DBPASS@controller/nova
"
sed -i "/$STARTING/,/^#/{/^[^#]/{//!d}}" /etc/nova/nova.conf
echo -n $APPEND | sed -i "/$STARTING/r /dev/stdin" /etc/nova/nova.conf
fi
STARTING="^\[api\]"
APPEND="
auth_strategy = keystone
"
sed -i "/$STARTING/,/^#/{/^[^#]/{//!d}}" /etc/nova/nova.conf
echo -n $APPEND | sed -i "/$STARTING/r /dev/stdin" /etc/nova/nova.conf
STARTING="^\[keystone_authtoken\]"
APPEND="
auth_url = http://controller:5000/v3
memcached_servers = controller:11211
auth_type = password
project_domain_name = Default
user_domain_name = Default
project_name = service
username = nova
password = $NOVA_PASS
"
sed -i "/$STARTING/,/^#/{/^[^#]/{//!d}}" /etc/nova/nova.conf
echo -n $APPEND | sed -i "/$STARTING/r /dev/stdin" /etc/nova/nova.conf

#Configure the [neutron] section of /etc/nova/nova.conf
STARTING="^\[neutron\]"
APPEND="
url = http://controller:9696
auth_url = http://controller:5000
auth_type = password
project_domain_name = default
user_domain_name = default
region_name = RegionOne
project_name = service
username = neutron
password = $NEUTRON_PASS
"
sed -i "/$STARTING/,/^#/{/^[^#]/{//!d}}" /etc/nova/nova.conf
echo -n $APPEND | sed -i "/$STARTING/r /dev/stdin" /etc/nova/nova.conf
ln -s /etc/apparmor.d/usr.sbin.dnsmasq /etc/apparmor.d/disable/

STARTING="^\[vnc\]"
APPEND="
enabled = true
server_listen = 0.0.0.0
server_proxyclient_address = \$my_ip
"
if grep -q compute <<<$NODE; then
APPEND=$APPEND+"
novncproxy_base_url = http://controller:6080/vnc_auto.html
"
fi
sed -i "/$STARTING/,/^#/{/^[^#]/{//!d}}" /etc/nova/nova.conf
echo -n $APPEND | sed -i "/$STARTING/r /dev/stdin" /etc/nova/nova.conf
STARTING="^\[glance\]"
APPEND="
api_servers = http://controller:9292
"
sed -i "/$STARTING/,/^#/{/^[^#]/{//!d}}" /etc/nova/nova.conf
echo -n $APPEND | sed -i "/$STARTING/r /dev/stdin" /etc/nova/nova.conf
STARTING="^\[oslo_concurrency\]"
APPEND="
lock_path = /var/lib/nova/tmp
"
sed -i "/$STARTING/,/^#/{/^[^#]/{//!d}}" /etc/nova/nova.conf
echo -n $APPEND | sed -i "/$STARTING/r /dev/stdin" /etc/nova/nova.conf
STARTING="^\[placement\]"
APPEND="
region_name = RegionOne
project_domain_name = Default
project_name = service
auth_type = password
user_domain_name = Default
auth_url = http://controller:5000/v3
username = placement
password = $PLACEMENT_PASS
"
sed -i "/$STARTING/,/^#/{/^[^#]/{//!d}}" /etc/nova/nova.conf
echo -n $APPEND | sed -i "/$STARTING/r /dev/stdin" /etc/nova/nova.conf

if grep -q controller <<<$NODE; then
su -s /bin/sh -c "nova-manage api_db sync" nova
su -s /bin/sh -c "nova-manage cell_v2 map_cell0" nova
su -s /bin/sh -c "nova-manage cell_v2 create_cell --name=cell1 --verbose" nova
su -s /bin/sh -c "nova-manage db sync" nova
su -s /bin/sh -c "nova-manage cell_v2 list_cells" nova
systemctl enable  openstack-nova-api.service openstack-nova-consoleauth openstack-nova-scheduler.service openstack-nova-conductor.service openstack-nova-novncproxy.service
systemctl restart openstack-nova-api.service openstack-nova-consoleauth openstack-nova-scheduler.service openstack-nova-conductor.service openstack-nova-novncproxy.service
#Add the compute node to the cell database¶
source admin-openrc
openstack compute service list --service nova-compute
su -s /bin/sh -c "nova-manage cell_v2 discover_hosts --verbose" nova
#Verify operation
source admin-openrc
openstack compute service list
openstack catalog list
openstack image list
nova-status upgrade check
else
STARTING="^\[libvirt\]"
if [ `egrep -c '(vmx|svm)' /proc/cpuinfo` >=1 ]; then
APPEND="
virt_type = kvm
"
else
APPEND="
virt_type = qemu
"
fi
sed -i "/$STARTING/,/^#/{/^[^#]/{//!d}}" /etc/nova/nova.conf
echo -n $APPEND | sed -i "/$STARTING/r /dev/stdin" /etc/nova/nova.conf
systemctl enable libvirtd.service openstack-nova-compute.service
systemctl start libvirtd.service openstack-nova-compute.service
fi

#2.4 Network service
if grep -q controller <<<$NODE; then
mysql -u root -p$ROOT_DBPASS <<EOF
CREATE DATABASE IF NOT EXISTS `neutron`;
GRANT ALL PRIVILEGES ON neutron.* TO 'neutron'@'localhost' IDENTIFIED BY '$NEUTRON_DBPASS';
GRANT ALL PRIVILEGES ON neutron.* TO 'neutron'@'%' IDENTIFIED BY '$NEUTRON_DBPASS';
EOF
source admin-openrc
openstack user create --domain default --password-prompt neutron
openstack role add --project service --user neutron admin
openstack service create --name neutron --description "OpenStack Networking" network
openstack endpoint create --region RegionOne network public http://controller:9696
openstack endpoint create --region RegionOne network internal http://controller:9696
openstack endpoint create --region RegionOne network admin http://controller:9696
fi
#Configure networking options
if grep -q controller <<<$NODE; then
yum install -y openstack-neutron openstack-neutron-ml2 openstack-neutron-linuxbridge ebtables
STARTING="^\[database\]"
APPEND="
connection = mysql+pymysql://neutron:$NEUTRON_DBPASS@controller/neutron
"
sed -i "/$STARTING/,/^#/{/^[^#]/{//!d}}" /etc/neutron/neutron.conf
echo -n $APPEND | sed -i "/$STARTING/r /dev/stdin" /etc/neutron/neutron.conf
else
yum install -y openstack-neutron-linuxbridge ebtables ipset
fi
STARTING="^\[DEFAULT\]"
APPEND="
transport_url = rabbit://openstack:$RABBIT_PASS@controller
auth_strategy = keystone
"
if grep -q controller <<<$NODE; then
APPEND=$APPEND+"
core_plugin = ml2
service_plugins = router
allow_overlapping_ips = true
notify_nova_on_port_status_changes = true
notify_nova_on_port_data_changes = true
"
fi
sed -i "/$STARTING/,/^#/{/^[^#]/{//!d}}" /etc/neutron/neutron.conf
echo -n $APPEND | sed -i "/$STARTING/r /dev/stdin" /etc/neutron/neutron.conf
STARTING="^\[keystone_authtoken\]"
APPEND="
www_authenticate_uri = http://controller:5000
auth_url = http://controller:5000
memcached_servers = controller:11211
auth_type = password
project_domain_name = default
user_domain_name = default
project_name = service
username = neutron
password = $NEUTRON_PASS
"
sed -i "/$STARTING/,/^#/{/^[^#]/{//!d}}" /etc/neutron/neutron.conf
echo -n $APPEND | sed -i "/$STARTING/r /dev/stdin" /etc/neutron/neutron.conf

STARTING="^\[oslo_concurrency\]"
APPEND="
lock_path = /var/lib/neutron/tmp
"
sed -i "/$STARTING/,/^#/{/^[^#]/{//!d}}" /etc/neutron/neutron.conf
echo -n $APPEND | sed -i "/$STARTING/r /dev/stdin" /etc/neutron/neutron.conf

if grep -q controller <<<$NODE; then
STARTING="^\[nova\]"
APPEND="
auth_url = http://controller:5000
auth_type = password
project_domain_name = default
user_domain_name = default
region_name = RegionOne
project_name = service
username = nova
password = $NOVA_PASS
"
sed -i "/$STARTING/,/^#/{/^[^#]/{//!d}}" /etc/neutron/neutron.conf
echo -n $APPEND | sed -i "/$STARTING/r /dev/stdin" /etc/neutron/neutron.conf

STARTING="^\[ml2\]"
APPEND="
type_drivers = flat,vlan,vxlan
tenant_network_types = vxlan
mechanism_drivers = linuxbridge,l2population
extension_drivers = port_security
"
sed -i "/$STARTING/,/^#/{/^[^#]/{//!d}}" /etc/neutron/plugins/ml2/ml2_conf.ini
echo -n $APPEND | sed -i "/$STARTING/r /dev/stdin" /etc/neutron/plugins/ml2/ml2_conf.ini
STARTING="^\[ml2_type_flat\]"
APPEND="
flat_networks = provider
vni_ranges = 1:1000
"
sed -i "/$STARTING/,/^#/{/^[^#]/{//!d}}" /etc/neutron/plugins/ml2/ml2_conf.ini
echo -n $APPEND | sed -i "/$STARTING/r /dev/stdin" /etc/neutron/plugins/ml2/ml2_conf.ini
STARTING="^\[securitygroup\]"
APPEND="
enable_ipset = true
"
sed -i "/$STARTING/,/^#/{/^[^#]/{//!d}}" /etc/neutron/plugins/ml2/ml2_conf.ini
echo -n $APPEND | sed -i "/$STARTING/r /dev/stdin" /etc/neutron/plugins/ml2/ml2_conf.ini

STARTING="^\[linux_bridge\]"
APPEND="
physical_interface_mappings = $PROVIDER_INTERFACE
"
sed -i "/$STARTING/,/^#/{/^[^#]/{//!d}}" /etc/neutron/plugins/ml2/linuxbridge_agent.ini
echo -n $APPEND | sed -i "/$STARTING/r /dev/stdin" /etc/neutron/plugins/ml2/linuxbridge_agent.ini
STARTING="^\[vxlan\]"
APPEND="
enable_vxlan = true
local_ip = $MANAGEMENT_IP
l2_population = true
"
sed -i "/$STARTING/,/^#/{/^[^#]/{//!d}}" /etc/neutron/plugins/ml2/linuxbridge_agent.ini
echo -n $APPEND | sed -i "/$STARTING/r /dev/stdin" /etc/neutron/plugins/ml2/linuxbridge_agent.ini
STARTING="^\[securitygroup\]"
APPEND="
enable_security_group = true
firewall_driver = neutron.agent.linux.iptables_firewall.IptablesFirewallDriver
"
sed -i "/$STARTING/,/^#/{/^[^#]/{//!d}}" /etc/neutron/plugins/ml2/linuxbridge_agent.ini
echo -n $APPEND | sed -i "/$STARTING/r /dev/stdin" /etc/neutron/plugins/ml2/linuxbridge_agent.ini

sed -i "/net.bridge.bridge-nf-call-iptables/d" /etc/sysctl.conf
sed -i "/net.bridge.bridge-nf-call-ip6tables/d" /etc/sysctl.conf
echo "net.bridge.bridge-nf-call-iptables=1" >> /etc/sysctl.conf
echo "net.bridge.bridge-nf-call-ip6tables=1" >> /etc/sysctl.conf
sysctl -p

STARTING="^\[DEFAULT\]"
APPEND="
interface_driver = linuxbridge
"
sed -i "/$STARTING/,/^#/{/^[^#]/{//!d}}" /etc/neutron/l3_agent.ini
echo -n $APPEND | sed -i "/$STARTING/r /dev/stdin" /etc/neutron/l3_agent.ini

STARTING="^\[DEFAULT\]"
APPEND="
interface_driver = linuxbridge
dhcp_driver = neutron.agent.linux.dhcp.Dnsmasq
enable_isolated_metadata = true
"
sed -i "/$STARTING/,/^#/{/^[^#]/{//!d}}" /etc/neutron/dhcp_agent.ini
echo -n $APPEND | sed -i "/$STARTING/r /dev/stdin" /etc/neutron/dhcp_agent.ini
fi

#Configure the metadata agent¶
STARTING="^\[DEFAULT\]"
APPEND="
nova_metadata_host = controller
metadata_proxy_shared_secret = $METADATA_SECRET
"
sed -i "/$STARTING/,/^#/{/^[^#]/{//!d}}" /etc/neutron/metadata_agent.ini 
echo -n $APPEND | sed -i "/$STARTING/r /dev/stdin" /etc/neutron/metadata_agent.ini 
ln -s /etc/neutron/plugins/ml2/ml2_conf.ini /etc/neutron/plugin.ini
su -s /bin/sh -c "neutron-db-manage --config-file /etc/neutron/neutron.conf --config-file /etc/neutron/plugins/ml2/ml2_conf.ini upgrade head" neutron
systemctl restart openstack-nova-api.service
systemctl enable neutron-server.service neutron-linuxbridge-agent.service neutron-dhcp-agent.service neutron-metadata-agent.service
systemctl restart neutron-server.service neutron-linuxbridge-agent.service neutron-dhcp-agent.service neutron-metadata-agent.service
systemctl enable neutron-l3-agent.service
systemctl restart neutron-l3-agent.service
fi
