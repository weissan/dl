#!/bin/sh
set -o errexit
set -o nounset
set -x

pip uninstall -y pycurl
export PYCURL_SSL_LIBRARY=nss
pip install pycurl  --no-cache-dir

yum erase -y salt

wget -O salt-repo-latest https://repo.saltstack.com/yum/redhat/salt-repo-latest-1.el7.noarch.rpm
rpm -Uvh --force salt-repo-latest

yum install -y salt-minion

id=`dig +short myip.opendns.com @resolver1.opendns.com`
sed -i -e "/^#\?master:/c master: st.curries.cc" -e "/^#\?id:/c id: $id" /etc/salt/minion

systemctl enable salt-minion
systemctl restart salt-minion
systemctl status -l salt-minion
