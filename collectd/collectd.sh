#!/bin/sh
set -o errexit
set -o nounset
set -x

yum install -y collectd
yum update  -y collectd

wget -O /etc/collectd.conf https://bitbucket.org/weissan/dl/raw/master/collectd/collectd.conf

hostname=`hostname`
ip=`dig +short myip.opendns.com @resolver1.opendns.com`
if echo $hostname | grep -q "^localhost"
then
id=$ip
else
id=$hostname
fi

sed -i -e "/^#\?Hostname/c Hostname\t\"$id\"" /etc/collectd.conf

systemctl enable collectd
systemctl restart collectd
