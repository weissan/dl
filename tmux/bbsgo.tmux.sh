#!/bin/bash
/bin/tmux new -n server -s bbsgo -d
/bin/tmux send -t "bbsgo:server" "cd /opt/git/bbs-go/server && ./bbs-go" Enter
/bin/tmux neww -n site -t bbsgo
/bin/tmux send -t "bbsgo:site" "cd /opt/git/bbs-go/site && npm run start" Enter
